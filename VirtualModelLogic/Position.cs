﻿using Helpers;

namespace VirtualModelLogic
{
    public class Position
    {
        public double Heading { get; set; }
        public double Tilt { get; set; }


        public Position() { }

        public Position(Vector v)
        {
            Heading = v.X;
            Tilt = v.Y;
        }

        public Position(double inHeading, double inTilt)
        {
            Heading = inHeading;
            Tilt = inTilt;
        }

        public override string ToString()
        {
            return $"H{Heading:F2}, T{Tilt:F2}";
        }
    }
}
