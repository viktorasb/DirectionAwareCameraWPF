﻿namespace VirtualModelLogic.Interfaces
{
    public interface ISensoryAggregate
    {
        double GetAzimuth();
        double GetTilt();
        double GetCameraRotation();

        Position GetCameraTarget();
    }
}