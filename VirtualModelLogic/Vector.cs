﻿using System;


namespace VirtualModelLogic
{
    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }


        public Vector() { }
        public Vector(double inX, double inY) { X = inX; Y = inY; }

        public void Normalize()
        {
            var length = Math.Sqrt(X * X + Y * Y);
            X = X / length;
            Y = Y / length;
        }


        public override string ToString()
        {
            return $"X[{X:F3}], Y[{Y:F3}]";
        }
    }
}
