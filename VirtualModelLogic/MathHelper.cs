﻿using System;
using Helpers;


namespace VirtualModelLogic
{
    public class MathHelper
    {
        public static double CalculateAngleBetween2Vectors(Vector v1, Vector v2)
        {
            v1.Normalize();
            v2.Normalize();
            double angle = Math.Atan2(v2.Y, v2.X) - Math.Atan2(v1.Y, v1.X);
            return (float)(angle);
        }

        public static double NormalizeAngle(double angle)
        {
            var pi2 = Math.PI * 2;
            // reduce the angle  
            var angleRepetitions = angle / pi2;
            if (Math.Abs(angleRepetitions) > 1)
                angle = angle - ((int)angleRepetitions) * pi2;

            // force it to be the positive remainder, so that 0 <= angle < 360  
            if (angle < 0)
                angle = angle + pi2;

            // force into the minimum absolute value residue class, so that -180 < angle <= 180  
            if (angle > Math.PI)
                angle -= pi2;

            return angle;
        }

    }
    
}
