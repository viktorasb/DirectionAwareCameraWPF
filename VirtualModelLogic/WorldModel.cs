﻿using System;
using System.Collections.Generic;
using Helpers;


namespace VirtualModelLogic
{
    public class WorldModel
    {
        private ISensoryAggregate _sensoryAggregate;
        private List<Augment> _augments;
        private DateTime _lastTimestamp = DateTime.Now;
        private ProjectionCalculator _projectionCalculator;

        public WorldModel(ISensoryAggregate inSensoryAggregate)
        {
            _augments = new List<Augment>();
            _sensoryAggregate = inSensoryAggregate;
            _projectionCalculator = new ProjectionCalculator();
        }


        public void FireBall()
        {
            var position = new Position();
            position.Heading =  MathHelper.NormalizeAngle(_sensoryAggregate.Heading);
            position.Tilt = MathHelper.NormalizeAngle(_sensoryAggregate.Tilt);
            var newAugment = new Augment() {WorldPosition = position};

            _augments.Add(newAugment);

        }


        public List<Augment> GetVisibleAugments(Position cameraAngleSpan, double deltaTime)
        { 
            var cameraTarget = _sensoryAggregate.CameraTarget;
            var cameraRotation = _sensoryAggregate.Rotation;

            _projectionCalculator.UpdatePositionalValues(cameraTarget, cameraAngleSpan, cameraRotation);

            List<Augment> visibleAugments = new List<Augment>();
            for (int i = 0; i < _augments.Count; i++)
            {
                _augments[i].Update(deltaTime);


                var screenPosition = _projectionCalculator.GetDistanceFromCenter(_augments[i].WorldPosition);
                if (Math.Abs(screenPosition.X) <= 1 && Math.Abs(screenPosition.Y) <= 1)
                {
                    _augments[i].ScreenPosition = screenPosition;
                    visibleAugments.Add(_augments[i]);
                }

            }

            return visibleAugments;
        }
    }
}
