﻿using Helpers;


namespace VirtualModelLogic
{
    public class Augment
    {
        public Position WorldPosition { get; set; }
        public Vector ScreenPosition { get; set; }

        private const double KLife = 2.5;
        public double Age { get; set; } = KLife;

        public bool WallHit { get; set; } = false;

        public double GetFadeRatio()
        {
            return Age / KLife;
        }

        public void Update(double dTime)
        {
            if (Age <= 0) return;

            Age -= dTime;
        }
    }
}
