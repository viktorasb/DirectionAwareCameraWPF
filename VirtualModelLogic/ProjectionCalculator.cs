﻿using Helpers;
using System;


namespace VirtualModelLogic
{
    class ProjectionCalculator
    {
        private double _cameraRotation;
        private Position _currentCameraPositoin;
        private Position _cameraAngleSpanRotated;
        private Position _cameraAngleSpan;

        public void UpdatePositionalValues(Vector currentCameraPosition, Position cameraAngleSpan, double cameraRotation)
        {
            var cameraRotationN = MathHelper.NormalizeAngle(cameraRotation);
            var currentCameraPositionN = new Position(MathHelper.NormalizeAngle(currentCameraPosition.X),
                                                      MathHelper.NormalizeAngle(currentCameraPosition.Y));

            //normalize input values
            var angleSpanAdjusted = new Position((1 + Math.Abs(Math.Sin(cameraRotation))) * cameraAngleSpan.Heading,
                                                 (1 + Math.Abs(Math.Cos(cameraRotation))) * cameraAngleSpan.Tilt);

            var cameraAngleSpanRotated = RotateCameraAngleSpan(angleSpanAdjusted, cameraRotation);

            _cameraRotation = cameraRotationN;
            _currentCameraPositoin = currentCameraPositionN;
            _cameraAngleSpan = angleSpanAdjusted;
            _cameraAngleSpanRotated = cameraAngleSpanRotated;
        }


        public Vector GetDistanceFromCenter(Position point)
        {
            return GetDistanceFromCenter(point, _currentCameraPositoin, _cameraAngleSpan, _cameraAngleSpanRotated, _cameraRotation);
        }

        private static Vector GetDistanceFromCenter(Position point, Position currentCameraPosition, Position cameraAngleSpan, Position cameraAngleSpanRotated, double cameraRotation)
        {
            var pointN = RecheckForWrapping(point, currentCameraPosition, cameraAngleSpanRotated);

            var pointProjection = new Vector(-3, -3);
            if (IsPointWithin(pointN, currentCameraPosition, cameraAngleSpan, cameraRotation))
                pointProjection = ProjectPointPosition(pointN, currentCameraPosition, cameraAngleSpan, cameraRotation);

            return pointProjection;
        }


        private static Position RotateCameraAngleSpan(Position originalAngleSpan, double cameraRotation)
        {
            return new Position(Math.Abs(originalAngleSpan.Heading * Math.Cos(cameraRotation) + originalAngleSpan.Tilt * Math.Sin(cameraRotation)),
                Math.Abs(originalAngleSpan.Heading * Math.Sin(cameraRotation) + originalAngleSpan.Tilt * Math.Cos(cameraRotation)));
        }

        private static Position RecheckForWrapping(Position point, Position currentCameraPosition, Position cameraAngleSpan)
        {

            Position adjustedPoint = new Position(point.Heading, point.Tilt);
            if (IsResultingHeadingOver180(currentCameraPosition, cameraAngleSpan))
                if (point.Heading < 0)
                    adjustedPoint.Heading = Math.PI * 2 + point.Heading;
            if (IsResultingHeadingBelowMinus180(currentCameraPosition, cameraAngleSpan))
                if (point.Heading > 0)
                    adjustedPoint.Heading = -Math.PI * 2 + point.Heading;

            if (IsResultingTiltAbove90(currentCameraPosition, cameraAngleSpan))
                if (point.Tilt < 0)
                    adjustedPoint.Tilt = Math.PI + point.Tilt;
            if (IsResultingTiltBelowMinus90(currentCameraPosition, cameraAngleSpan))
                if (point.Tilt > 0)
                    adjustedPoint.Tilt = -Math.PI + point.Tilt;

            return adjustedPoint;
        }



#region Wrapping conditional checks
        private static bool IsResultingHeadingOver180(Position currentCameraPosition, Position cameraAngleSpan)
        {
            return currentCameraPosition.Heading > 0 && currentCameraPosition.Heading + cameraAngleSpan.Heading / 2 > Math.PI;
        }

        private static bool IsResultingTiltBelowMinus90(Position currentCameraPosition, Position cameraAngleSpan)
        {
            return currentCameraPosition.Tilt < 0 && currentCameraPosition.Tilt - cameraAngleSpan.Tilt / 4 < -Math.PI / 2;
        }

        private static bool IsResultingTiltAbove90(Position currentCameraPosition, Position cameraAngleSpan)
        {
            return currentCameraPosition.Tilt > 0 && currentCameraPosition.Tilt + cameraAngleSpan.Tilt / 4 > Math.PI / 2;
        }

        private static bool IsResultingHeadingBelowMinus180(Position currentCameraPosition, Position cameraAngleSpan)
        {
            return currentCameraPosition.Heading < 0 && currentCameraPosition.Heading - cameraAngleSpan.Heading / 2 < -Math.PI;
        }
#endregion

        private static bool IsPointWithin(Position pointPosition, Position currentCameraTargetPosition,
                                          Position cameraAngleSpan, double angle)
        {
            var camAngleSpanH = cameraAngleSpan.Heading;
            var camAngleSpanV = cameraAngleSpan.Tilt;

            double cameraHeading = MathHelper.NormalizeAngle(currentCameraTargetPosition.Heading);
            double cameraTilt = currentCameraTargetPosition.Tilt;

            //create a viewplane rectangle using current center point and camera angle of view
            Position[] rectangleO = new Position[4];
            rectangleO[0] = new Position(cameraHeading - camAngleSpanH / 2, cameraTilt - camAngleSpanV / 2);
            rectangleO[1] = new Position(cameraHeading + camAngleSpanH / 2, cameraTilt - camAngleSpanV / 2);
            rectangleO[2] = new Position(cameraHeading + camAngleSpanH / 2, cameraTilt + camAngleSpanV / 2);
            rectangleO[3] = new Position(cameraHeading - camAngleSpanH / 2, cameraTilt + camAngleSpanV / 2);

            //rotate all four corners of the rectangle around the center (if camera was not held perfectly flat)
            Position[] reactangleR = new Position[4];
            for (int i = 0; i < 4; i++)
            {
                Vector originalV = new Vector(rectangleO[i].Heading, rectangleO[i].Tilt);
                Vector rotatedV = Vector.RotatePoint(originalV, new Vector(cameraHeading, cameraTilt), angle);
                reactangleR[i] = new Position(rotatedV.X, rotatedV.Y);
            }

            //check if the given point is within the viewplane rectangle
            double pointHeading = pointPosition.Heading;
            double pointTilt = pointPosition.Tilt;

            bool pointWithin = false;
            int expectedSign = 0;
            for (int i = 0; i < 4; i++)
            {
                int i0 = i;
                int i1 = (i + 1) % 4;
                double A = 0.5f * (reactangleR[i1].Heading * pointTilt - reactangleR[i1].Tilt * pointHeading 
                                   - reactangleR[i0].Heading * pointTilt + reactangleR[i0].Tilt * pointHeading
                                   + reactangleR[i0].Heading * reactangleR[i1].Tilt
                                   - reactangleR[i0].Tilt * reactangleR[i1].Heading);
                if (i == 0)
                    expectedSign = Math.Sign(A);

                //check and break out if the values don't correspond. neat little trick
                if ((A >= 0 && expectedSign >= 0) || (A < 0 && expectedSign < 0))
                {
                    if (i == 3)
                        pointWithin = true;
                }
                else break;

            }

            return pointWithin;
        }

        private static Vector ProjectPointPosition(Position pointPosition, Position currentCameraTargetPosition, Position cameraAngleSpan, double angle)
        {
            var pointPositionV = new Vector(pointPosition.Heading, pointPosition.Tilt);
            var currentCameraTargetPositionV = new Vector(currentCameraTargetPosition.Heading,
                                                          currentCameraTargetPosition.Tilt);

            //rotate point backwards for easy projection
            var rotatedPoint = Vector.RotatePoint(pointPositionV, currentCameraTargetPositionV, -angle);
            var pointNoOffset = new Vector(rotatedPoint.X - currentCameraTargetPosition.Heading + cameraAngleSpan.Heading / 2,
                                           rotatedPoint.Y - currentCameraTargetPosition.Tilt + cameraAngleSpan.Tilt / 2);
            var finalizedPosition = new Vector(pointNoOffset.X / cameraAngleSpan.Heading,
                                               1 - pointNoOffset.Y / cameraAngleSpan.Tilt);

            return finalizedPosition;
        }
    }
}
