﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Media.Capture;
using Windows.UI.Xaml.Controls;
using Panel = Windows.Devices.Enumeration.Panel;

namespace FacingAware_WPF.CameraWorkings
{
    class CameraCapturer
    {
        private DeviceInformation _camera;
        private CaptureElement _captureSink;
        private MediaCapture _mediaCapture;

        public CameraCapturer(CaptureElement inCaptureElement)
        {
            _captureSink = inCaptureElement;

            InitializeCamera();
        }

        ~CameraCapturer()
        {
            _mediaCapture.StopPreviewAsync();
            _mediaCapture.Dispose();
        }

        private async void InitializeCamera()
        {
            DeviceInformationCollection webcams;
            webcams = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);

            DeviceInformation backCamera = (from webcam in webcams
                                            where webcam.EnclosureLocation != null && webcam.EnclosureLocation.Panel == Panel.Back
                                            select webcam).FirstOrDefault();

            _mediaCapture = new MediaCapture();
            
            _camera = backCamera;

            await StartCapture();
        }

        
        public async Task StartCapture()
        {
            try
            {
                var settings = new MediaCaptureInitializationSettings() { VideoDeviceId = _camera.Id };
                await _mediaCapture.InitializeAsync(settings);

                _captureSink.Source = _mediaCapture;

                await _mediaCapture.StartPreviewAsync();

            }
            catch (Exception e)
            {
                await _mediaCapture.StopPreviewAsync();
                _mediaCapture.Dispose();

                throw e;
            }

        }
    }
}
