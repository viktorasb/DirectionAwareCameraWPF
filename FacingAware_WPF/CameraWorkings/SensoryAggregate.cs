﻿using System;
using System.ComponentModel;
using VirtualModelLogic;
using VirtualModelLogic.Interfaces;
using Windows.Devices.Sensors;
using Windows.UI.Core;

namespace FacingAware_WPF.CameraWorkings
{
    public class SensoryAggregate : ISensoryAggregate, INotifyPropertyChanged
    {
        private Accelerometer _accelerometer;
        private Magnetometer _magnetometer;
        private Gyrometer _gyrometer;
        private Compass _compass;

        private FilteredSensor _accelFiltered = new FilteredSensor();
        private FilteredSensor _compassFiltered = new FilteredSensor();

        public string GyroReadings { get; set; }
        public string AccelerometerReadings { get; set; }
        public string MagnetometerReadings { get; set; }
        public string CompassReadings { get; set; }
        public string CameraRotation { get; set; }
        public string IntegratedReading { get; set; }


        public SensoryAggregate()
        {
            InitializeSensors();
        }

        private void InitializeSensors()
        {
            _gyrometer = Gyrometer.GetDefault();
            _magnetometer = Magnetometer.GetDefault();
            _accelerometer = Accelerometer.GetDefault();
            _compass = Compass.GetDefault();

            _gyrometer.ReadingChanged += _gyrometer_ReadingChanged;
            _magnetometer.ReadingChanged += _magnetometer_ReadingChanged;
            _accelerometer.ReadingChanged += _accelerometer_ReadingChanged;
            _compass.ReadingChanged += _compass_ReadingChanged;
            _compass.ReportInterval = 20;
        }

        private double NormalizeHeading(double heading)//pabandom tai padaryti per rotationa
        {
            var rotation = GetCameraRotation();

            if (rotation > Math.PI / 4 * 7 || rotation < Math.PI / 4)
                return heading;

            if (rotation > Math.PI / 4 && rotation < Math.PI / 4 * 3)
                return heading - Math.PI / 2;

            if (rotation > Math.PI / 4 * 3 && rotation < Math.PI / 4 * 5)
                return heading + Math.PI;

            if (rotation > Math.PI / 4 * 5 && rotation < Math.PI / 4 * 7)
                return heading - Math.PI / 2;

            throw new InvalidOperationException("Shouldn't happen. but it happened. have fun!");
        }

        public double GetAzimuth()//prideti gyroscopo nuskaitymus
        {
            var compassReading = _compass.GetCurrentReading();

            var azimuth = compassReading.HeadingMagneticNorth / 180.0 * Math.PI;

            var normalized = -NormalizeHeading(azimuth);

            return _compassFiltered.FilterValue(normalized) ;
        }


        public double GetTilt()
        {
            return GAIntegratedTilt(GetTiltOld(), _gyrometer.GetCurrentReading().AngularVelocityY/180.0*Math.PI);
        }

        public double GetTiltOld()//prideti gyroscopo nuskaitymus
        {
            
            var currentReading = _accelerometer.GetCurrentReading();
            var tilt = currentReading.AccelerationZ * Math.PI / 2;
            return _accelFiltered.FilterValue(-tilt);
        }

        public double GetCameraRotation()//buttonai desinej pusej - rotationas 0
        {
            var readings = _accelerometer.GetCurrentReading();

            var v1 = new Vector(-1, 0);
            var v2 = new Vector(readings.AccelerationX, readings.AccelerationY);

            var angle = MathHelper.CalculateAngleBetween2Vectors(v2, v1);

            return angle;
        }

        public Position GetCameraTarget()
        {
            return new Position(GetAzimuth(), GetTilt());
        }

        private void _compass_ReadingChanged(Compass sender, CompassReadingChangedEventArgs args)
        {
            CompassReadings = $"Comp: North{GetAzimuth()},  Accuracy{args.Reading.HeadingAccuracy}";
            OnPropertyChanged("CompassReadings");
        }

        private void _accelerometer_ReadingChanged(Accelerometer sender, AccelerometerReadingChangedEventArgs args)
        {
            AccelerometerReadings = $"Accl: X{args.Reading.AccelerationX:F2}, Y{args.Reading.AccelerationY:F2}, Z{args.Reading.AccelerationZ:F2}";
            OnPropertyChanged("AccelerometerReadings");

            CameraRotation = $"Rot: {GetCameraRotation():F2}";
            OnPropertyChanged("CameraRotation");
        }

        private void _magnetometer_ReadingChanged(Magnetometer sender, MagnetometerReadingChangedEventArgs args)
        {
            MagnetometerReadings =
                $"Magn: X{args.Reading.MagneticFieldX:F2}, Y{args.Reading.MagneticFieldY:F2}, Z{args.Reading.MagneticFieldZ:F2},     Reliability:{args.Reading.DirectionalAccuracy}";
            OnPropertyChanged("MagnetometerReadings");
        }

        private void _gyrometer_ReadingChanged(Gyrometer sender, GyrometerReadingChangedEventArgs args)
        {
            GyroReadings =
                $"Gyro: X{args.Reading.AngularVelocityX:F2}, Y{args.Reading.AngularVelocityY:F2}, Z{args.Reading.AngularVelocityZ:F2}";
            OnPropertyChanged("GyroReadings");


            var tlt = GetTiltOld();

            IntegratedReading = $"-=Integrated=-  {GAIntegratedTilt(tlt, args.Reading.AngularVelocityY / 180.0 * Math.PI ):F2}";
    
            OnPropertyChanged("IntegratedReading");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            () =>
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

            }
            );

        }



        double filterAngle = 0;
        
        private DateTime lastTime = DateTime.Now;

        private double GAIntegratedTilt(double newAngle, double newRate)
        {

            var currTime = DateTime.Now;
            var dt = (currTime - lastTime).TotalMilliseconds/1000.0;
            lastTime = currTime;

            double filterTerm0;
            double filterTerm1;
            double filterTerm2 = 0;
            double timeConstant;

            timeConstant = 0.5; // default 1.0

            filterTerm0 = (newAngle - filterAngle) * timeConstant * timeConstant;
            filterTerm2 += filterTerm0 * dt;
            filterTerm1 = filterTerm2 + ((newAngle - filterAngle) * 2 * timeConstant) + newRate;
            filterAngle = (filterTerm1 * dt) + filterAngle;

            return filterAngle; // This is actually the current angle, but is stored for the next iteration
        }

    }
}