﻿using FacingAware_WPF.CameraWorkings.Visualization;
using System;
using System.ComponentModel;
using VirtualModelLogic;
using Windows.UI.Xaml.Controls;
using SensorReading;

namespace FacingAware_WPF.CameraWorkings
{
    class AugmentedCamera : INotifyPropertyChanged
    {
        private const double KHorizontalCameraAngleRange = 66 / 180.0 * Math.PI;//↔66
        private const double KVerticalCameraAngleRange = 40 / 180.0 * Math.PI;//↕40
        private readonly Position KCameraAngles = new Position(KHorizontalCameraAngleRange, KVerticalCameraAngleRange);




        private CameraCapturer _capturer;
        private WorldModel _worldModel;
        private ReadingsCollection _readingsCollection;
        private Visualizer _visualizer;

        public string Reliability => _readingsCollection.Reliability;
        public string ConsoleOut { get; private set; }//"Console out" to quickly output data onscreen

        public AugmentedCamera(CaptureElement captureElement, Canvas augmentsOverlay)
        {
            _readingsCollection = new ReadingsCollection();
            _worldModel = new WorldModel(_readingsCollection);

            _capturer = new CameraCapturer(captureElement);
            _visualizer = new Visualizer(augmentsOverlay);
        }

        public void UpdateAugments(double dTime)
        {
            _readingsCollection.Update((float)dTime);


            var rotation = _readingsCollection.Rotation;

            var augments = _worldModel.GetVisibleAugments(KCameraAngles, dTime);
            _visualizer.DrawAugments(augments);
            OnPropertyChanged("Reliability");
            
            ConsoleOut = $" ↔ {_readingsCollection.Heading / Math.PI * 180}\n ↕ {_readingsCollection.Tilt / Math.PI * 180}";
            OnPropertyChanged("ConsoleOut");
        }

        public void FireBall()
        {
            _worldModel.FireBall();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
