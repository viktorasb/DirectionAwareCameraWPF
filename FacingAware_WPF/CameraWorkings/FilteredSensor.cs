﻿using System.Collections.Generic;


namespace FacingAware_WPF.CameraWorkings
{
    class FilteredSensor //currently implemented as moving average
    {

        private const int KKeepKount = 1;

        private readonly List<double> _values= new List<double>();

        
        public double FilterValue(double inValue)
        {
            double filtered = 0;

            lock (_values)
            {
                if (_values.Count >= KKeepKount)
                    _values.RemoveAt(0);

                _values.Add(inValue);

                
                foreach (var value in _values)
                    filtered += value;

                filtered = filtered / _values.Count;
            }


            return filtered;
        }
    }
}
