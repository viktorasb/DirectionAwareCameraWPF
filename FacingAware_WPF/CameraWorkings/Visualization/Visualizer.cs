﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using VirtualModelLogic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace FacingAware_WPF.CameraWorkings.Visualization
{
    class Visualizer
    {

        private readonly BitmapImage KHoleImage = new BitmapImage(new Uri("ms-appx:///Assets/Hole.png"));
        private readonly BitmapImage KBallImage = new BitmapImage(new Uri("ms-appx:///Assets/Baseball.png"));
        private const double KImageOriginalSize = 50;

        private Dictionary<Augment, Image> _images;
        private readonly Canvas _augmentLayer;

        public Visualizer(Canvas inAugmentsLayer)
        {
            _augmentLayer = inAugmentsLayer;
            _images = new Dictionary<Augment, Image>();
        }

        public void DrawAugments(List<Augment> augments)
        {
            foreach (var imagePair in _images)
                imagePair.Value.Visibility = Visibility.Collapsed;

            
    
            for (int i = 0; i < augments.Count; i++)
            {
                var augment = augments[i];
                var pos = augment.ScreenPosition;

                Image image = null;
                if (_images.ContainsKey(augment))
                    image = _images[augment];

                if (image == null && augment.Age > 0)
                {
                    var newImage = CreateBall();
                    _augmentLayer.Children.Add(newImage);
                    image = newImage;
                    _images.Add(augment, image);
                }

                if (!augment.WallHit && augment.Age <= 0)
                {
                    var newImage = CreateHole();
                    _augmentLayer.Children.Add(newImage);
                    image = newImage;
                    _images.Remove(augment);
                    _images.Add(augment, image);
                    augment.WallHit = true;
                }

                
                image.Visibility = Visibility.Visible;
                if (augment.Age > 0)
                {
                    image.Height = image.Width = KImageOriginalSize * augment.GetFadeRatio();
                }

                Canvas.SetLeft(image, _augmentLayer.ActualWidth  * pos.X - image.Width / 2);
                Canvas.SetTop(image,  _augmentLayer.ActualHeight * pos.Y - image.Height / 2);
            }

        }

        private Image CreateHole()
        {
            var image = new Image
            {
                Source = KHoleImage,
                Width = KImageOriginalSize,
                Height = KImageOriginalSize
            };

            return image;
        }


        private Image CreateBall()
        {
            var image = new Image
            {
                Source = KBallImage,
                Width = KImageOriginalSize,
                Height = KImageOriginalSize
            };

            return image;
        }

    }
}
