﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using FacingAware_WPF.CameraWorkings;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace FacingAware_WPF
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        private DispatcherTimer _updateTimer;
        private AugmentedCamera _augmentedCamera;
        private DateTime _lastTime;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            InitializeAugmentedCameraElements();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void InitializeAugmentedCameraElements()
        {
            _augmentedCamera = new AugmentedCamera(cameraView, markOverlay);

            DataContext = _augmentedCamera;

            _updateTimer = new DispatcherTimer();
            _updateTimer.Interval = TimeSpan.FromSeconds(0.03);
            _updateTimer.Tick += UpdateTimerTick;
            _updateTimer.Start();
            _lastTime = DateTime.Now;

           
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            _augmentedCamera.FireBall();
        }


        private void UpdateTimerTick(object sender, object e)
        {
            var timeNow = DateTime.Now;
            var timeDiff = (timeNow - _lastTime).TotalSeconds;
            _lastTime = timeNow;

            _augmentedCamera.UpdateAugments(timeDiff);

            //?is this OK?
            Canvas.SetTop(crosshairImage, markOverlay.ActualHeight / 2 - crosshairImage.ActualHeight / 2);
            Canvas.SetLeft(crosshairImage, markOverlay.ActualWidth / 2 - crosshairImage.ActualWidth / 2);
            //maybe move to OnNavigatedTo?
        }
    }
}