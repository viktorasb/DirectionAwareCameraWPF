﻿using System;

namespace Helpers
{
    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }


        public Vector() { }

        public Vector(double inX, double inY) { X = inX; Y = inY; }

        public void Normalize()
        {
            var length = Math.Sqrt(X * X + Y * Y);
            X = X / length;
            Y = Y / length;
        }


        public static Vector RotatePoint(Vector pointToRotate, Vector anchorPoint, double angleRadians)//clockwise rotation
        {
            Vector rotated = new Vector();

            double sin = Math.Sin(angleRadians);
            double cos = Math.Cos(angleRadians);

            rotated.X = (pointToRotate.X - anchorPoint.X) * cos - (anchorPoint.Y - pointToRotate.Y) * sin + anchorPoint.X;
            rotated.Y = (pointToRotate.Y - anchorPoint.Y) * cos - (pointToRotate.X - anchorPoint.X) * sin + anchorPoint.Y;


            return rotated;

        }

        public override string ToString()
        {
            return $"X[{X:F3}], Y[{Y:F3}]";
        }
    }
}
