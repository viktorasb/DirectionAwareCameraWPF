﻿namespace Helpers
{
    public interface ISensoryAggregate
    {
        double Heading { get; }
        double Tilt { get; }
        double Rotation { get; }

        Vector CameraTarget { get; }

        string Reliability { get; }
    }
}