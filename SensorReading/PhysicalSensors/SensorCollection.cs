﻿using System;
using Windows.Devices.Sensors;

namespace SensorReading.PhysicalSensors
{
    class SensorCollection
    {
        private readonly Gyrometer _gyro;
        private readonly Accelerometer _accl;
        private readonly Compass _comp;


        public CompassReading CompassReading { get; private set; }
        public GyrometerReading GyroscopeReading { get; private set; }
        public AccelerometerReading AccelerometerReading { get; private set; }

        public string Reliability { get; private set; } = "Unknown";

        public SensorCollection()
        {
            _gyro = Gyrometer.GetDefault();
            _accl = Accelerometer.GetDefault();
            _comp = Compass.GetDefault();
            _comp.ReportInterval = 1;
            _comp.ReadingChanged += CompOnReadingChanged;

            CompassReading = _comp.GetCurrentReading();
            AccelerometerReading = _accl.GetCurrentReading();
            GyroscopeReading = _gyro.GetCurrentReading();
        }

        private void CompOnReadingChanged(Compass sender, CompassReadingChangedEventArgs args)
        {
            lock (thisLock)
            {
                CompassReading = args.Reading;
                Reliability = CompassReading.HeadingAccuracy.ToString();
            }
        } Object thisLock = new Object();

        public void UpdateSensors()
        {
            AccelerometerReading = _accl.GetCurrentReading();
            GyroscopeReading = _gyro.GetCurrentReading();
        }
    }
}
