﻿using Helpers;
using SensorReading.AdaptedSensors;
using SensorReading.PhysicalSensors;

namespace SensorReading
{
    public class ReadingsCollection : ISensoryAggregate
    {
        public Vector CameraTarget
        {
            get { return new Vector(Heading, Tilt); }
        }
        public double Rotation => _rotationSensor.ValueRadian;
        public string Reliability => _sensorCollection.Reliability;
        public double Heading => _headingSensor.ValueRadian;
        public double Tilt => _tiltSensor.ValueRadian;


        private readonly SensorCollection _sensorCollection;

        private readonly TiltSensor _tiltSensor;
        private readonly HeadingSensor _headingSensor;
        private readonly RotationSensor _rotationSensor;

        public ReadingsCollection()
        {
            _sensorCollection = new SensorCollection();

            _tiltSensor = new TiltSensor(_sensorCollection);
            _headingSensor = new HeadingSensor(_sensorCollection);
            _rotationSensor = new RotationSensor(_sensorCollection);
        }

        public void Update(float dTime)
        {
            _sensorCollection.UpdateSensors();

            _tiltSensor.Update(dTime);
            _headingSensor.Update(dTime);
            _rotationSensor.Update(dTime);
        }
    }
}
