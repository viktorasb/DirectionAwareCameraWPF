﻿using System;
using Windows.Devices.Sensors;

namespace SensorReading.Filtering
{
    class Filter
    {
        private double _upperLimit;
        private double _lowerLimit;
        private bool _wrapValues;
        public Filter(double inLowerLimit, double inUpperLimit, bool inWrapValues)
        {
            _upperLimit = inUpperLimit;
            _lowerLimit = inLowerLimit;
            _wrapValues = inWrapValues;
        }


        public double FilterSensor(double fastReading, double reliableReading, double dTime, double currentAngle, AccelerometerReading accelerometerReading, bool filterOutSpikes)
        {
            if (!filterOutSpikes)
                accelerometerReading = null;

            double filtered = 0;

            var reliableReadingRechecked = CheckWrapDirection(currentAngle, reliableReading);

            filtered = FilterSensorInternal(fastReading, reliableReadingRechecked, dTime, currentAngle, accelerometerReading);

            filtered = LimitValue(filtered, _lowerLimit, _upperLimit, _wrapValues);

            return filtered;
        }



        private double FilterSensorInternal(double fastReading, double reliableReading, double dTime, double currentAngle, AccelerometerReading reading)
        {

            currentAngle += fastReading * dTime;

            var forceMagnitude = 0.0;
            if (reading != null)
                forceMagnitude = Math.Abs(reading.AccelerationX) + Math.Abs(reading.AccelerationY) + Math.Abs(reading.AccelerationZ);

            if ((forceMagnitude > 0.5 && forceMagnitude < 1.7) || reading == null)
            {
                // Turning around the X axis results in a vector on the Y-axis

                currentAngle = currentAngle * 0.85 + reliableReading * 0.15;
            }

            return currentAngle;
        }

        private double CheckWrapDirection(double value, double target)
        {
            if (_wrapValues)
            {
                var amount = _upperLimit - _lowerLimit;
                if (Math.Abs(value - target) > amount / 2)
                {
                    if (target > value)
                        return -(_upperLimit - target) + _lowerLimit;
                    return -(_lowerLimit - target) + _upperLimit;
                }
            }

            return target;
        }

        public static double LimitValue(double value, double lowerLimit, double upperLimit, bool wrapAround)
        {

            if (value > upperLimit)
            {
                if (wrapAround)
                    return value - upperLimit + lowerLimit;
                return upperLimit;
            }
            if (value < lowerLimit)
            {
                if (wrapAround)
                    return upperLimit - lowerLimit + value;
                return lowerLimit;
            }
            return value;
        }
    }
}
