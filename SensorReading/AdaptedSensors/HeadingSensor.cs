﻿using System;
using SensorReading.PhysicalSensors;
using Helpers;

namespace SensorReading.AdaptedSensors
{
    class HeadingSensor : SensorBase
    {
        private enum EQuadrants
        {
            QUAD_Undefined,

            QUAD_Normal,
            QUAD_Rotated,
            QUAD_Inverted,
        };


        private EQuadrants _currentQuadrant = EQuadrants.QUAD_Normal;
        private double _lastHeading = 0;

        public HeadingSensor(SensorCollection inSensorCollection) : base(inSensorCollection, 0, 360, true)
        {
            _value = _sensorCollection.CompassReading.HeadingMagneticNorth;
            _lastHeading = _value;
     
        }


        public override void Update(float dTime)
        {
            var accData = new Vector((float)_sensorCollection.AccelerometerReading.AccelerationX, (float)_sensorCollection.AccelerometerReading.AccelerationY);
            accData.Normalize();

            
            var combinedGyro = accData.X * _sensorCollection.GyroscopeReading.AngularVelocityX
                             + accData.Y * _sensorCollection.GyroscopeReading.AngularVelocityY;
            


            var heading = _sensorCollection.CompassReading.HeadingMagneticNorth;
            heading = NormalizeCompassHeading(heading, combinedGyro);


            _value = _filter.FilterSensor(combinedGyro, heading, dTime, _value, _sensorCollection.AccelerometerReading, false);
        }


        private double NormalizeCompassHeading(double heading, double combinedGyro)
        {
            
            //check for change
            const double changeTreshold = 70;
            bool overflow = Math.Abs(heading - _lastHeading) > 180;

            if (overflow)
            {
                if (heading < _lastHeading)
                    heading += 360;
                else _lastHeading += 360;
            }


            var diff = heading - _lastHeading;

            if (Math.Abs(diff) > changeTreshold)
            {
                if (diff > 0)
                {
                    if (_currentQuadrant == EQuadrants.QUAD_Normal)
                        _currentQuadrant = EQuadrants.QUAD_Rotated;
                    else if (_currentQuadrant == EQuadrants.QUAD_Rotated)
                        _currentQuadrant = EQuadrants.QUAD_Inverted;
                }
                else
                {
                    if (_currentQuadrant == EQuadrants.QUAD_Inverted)
                        _currentQuadrant = EQuadrants.QUAD_Rotated;
                    else if (_currentQuadrant == EQuadrants.QUAD_Rotated)
                        _currentQuadrant = EQuadrants.QUAD_Normal;
                }
            }

            double offset = 0;
            //modify values
            switch (_currentQuadrant)
            {
                case EQuadrants.QUAD_Rotated:
                    offset = -90;
                    break;

                case EQuadrants.QUAD_Inverted:
                    offset = 180;
                    break;
            }

            _lastHeading = heading;

            return heading + offset;
        }

    }
}
