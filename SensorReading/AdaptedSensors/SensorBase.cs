﻿using System;
using Helpers;
using SensorReading.Filtering;
using SensorReading.PhysicalSensors;

namespace SensorReading.AdaptedSensors
{
    abstract class SensorBase
    {
        public double ValueDegrees
        {
            get { return _value; }
        }
        public double ValueRadian
        {
            get { return _value / 180.0 * Math.PI; }
        }

        protected double _value;
        protected readonly SensorCollection _sensorCollection;

        protected Filter _filter;

        protected SensorBase(SensorCollection inSensorCollection, double inLowerLimit, double inUpperLimit, bool wrapAround)
        {
            _sensorCollection = inSensorCollection;
            _filter = new Filter(inLowerLimit, inUpperLimit, wrapAround);
        }

        public abstract void Update(float dTime);

        protected float CalcAngle2V(Vector v1, Vector v2)
        {
            v1.Normalize();
            v2.Normalize();
            double angle = Math.Atan2(v2.Y, v2.X) - Math.Atan2(v1.Y, v1.X);
            return (float)(angle / Math.PI * 180);
        }
    }
}
