﻿using Helpers;
using SensorReading.PhysicalSensors;

namespace SensorReading.AdaptedSensors
{
    class RotationSensor : SensorBase
    {
        public RotationSensor(SensorCollection inSensorCollection) : base(inSensorCollection, 0, 360, true)
        {
            _value = GetAngle();
        }

        public override void Update(float dTime)
        {
            var gyroValue = - _sensorCollection.GyroscopeReading.AngularVelocityZ;

            var accRotation = GetAngle();

            _value = _filter.FilterSensor(gyroValue, accRotation, dTime, _value, _sensorCollection.AccelerometerReading, true);
        }

        private double GetAngle()
        {
            return 360 - CalcAngle2V(new Vector(_sensorCollection.AccelerometerReading.AccelerationX, _sensorCollection.AccelerometerReading.AccelerationY), new Vector(-1, 0));
        }
    }

}
