﻿using Helpers;
using SensorReading.PhysicalSensors;

namespace SensorReading.AdaptedSensors
{
    class TiltSensor : SensorBase
    {
        public TiltSensor(SensorCollection inSensorCollection) : base(inSensorCollection, -90, 90, false)
        {
            _value = -_sensorCollection.AccelerometerReading.AccelerationZ;
        }

        public override void Update(float dTime)
        {
            var accZ = _sensorCollection.AccelerometerReading.AccelerationZ * 90;
            var accData = new Vector((float)_sensorCollection.AccelerometerReading.AccelerationX, (float)_sensorCollection.AccelerometerReading.AccelerationY);
            accData.Normalize();

            var combinedGyro = accData.X * _sensorCollection.GyroscopeReading.AngularVelocityY
                             + -accData.Y * _sensorCollection.GyroscopeReading.AngularVelocityX;

            _value = _filter.FilterSensor(combinedGyro, accZ, dTime, _value, _sensorCollection.AccelerometerReading, true);
        }

    }
}
